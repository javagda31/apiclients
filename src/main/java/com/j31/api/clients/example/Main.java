package com.j31.api.clients.example;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        // GET      -- pobieranie
        // POST     -- aktualizacji
        // PUT      -- podmiana/wstawianie
        // DELETE   -- usuwanie
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        // request/zapytanie - ramka http
        HttpRequest categoriesRequest = createCategoriesRequest();

        try {
            // wyślij zapytanie o kategorie
            HttpResponse<String> categoriesResponse = httpClient.send(
                    categoriesRequest,
                    HttpResponse.BodyHandlers.ofString());

            // wyciągnij z odpowiedzi Listę kategorii
            List<String> categories = objectMapper.readValue(categoriesResponse.body(),
                    new TypeReference<List<String>>() {
                    });

            // wypisz wszystkie kategorie na ekran
            System.out.println("Choose category:");
            for (int i = 0; i < categories.size(); i++) {
                System.out.println((i + 1) + ". " + categories.get(i));
            }

            // todo: podmienić na pytanie
            String kategoria = categories.get(3);
            int ilośćDowcipów = 10;

            List<ChuckNorrisResponse> jokes = requestJokes(kategoria, ilośćDowcipów, httpClient);
            for (int i = 0; i < jokes.size(); i++) {
                System.out.println((i + 1) + ". " + jokes.get(i).getValue());
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static List<ChuckNorrisResponse> requestJokes(String category,
                                                          int number,
                                                          HttpClient client) {
        // Stworzenie listy którą zwrócę na koniec w wyniku metody
        List<ChuckNorrisResponse> responses = new ArrayList<>();

        // stworzenie requestu
        HttpRequest request = createJokeRequest(category);

        // mapper
        ObjectMapper mapper = new ObjectMapper();

        // wczytujemy n dowcipów
        for (int i = 0; i < number; i++) {
            try {
                // wywołanie zapytania
                HttpResponse<String> resp = client.send(request, HttpResponse.BodyHandlers.ofString());

                // odczytanie i parsowanie odpowiedzi do obiektu.
                ChuckNorrisResponse chuckNorrisResponse = mapper.readValue(resp.body(), ChuckNorrisResponse.class);

                // dodanie obiektu do listy
                responses.add(chuckNorrisResponse);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return responses;
    }

    public static HttpRequest createCategoriesRequest() {
        return HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://api.chucknorris.io/jokes/categories"))
                .build();
    }

    public static HttpRequest createJokeRequest(String category) {
        return HttpRequest.newBuilder()
                .GET()
                .header("Accept", "application/json")
                .uri(URI.create("https://api.chucknorris.io/jokes/random?category=" + category))
                .build();
    }

}
