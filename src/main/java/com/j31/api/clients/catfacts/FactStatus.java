package com.j31.api.clients.catfacts;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FactStatus {
    private boolean verified;
    private int sentCount;
}
