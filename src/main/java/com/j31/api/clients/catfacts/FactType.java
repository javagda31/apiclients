package com.j31.api.clients.catfacts;

public enum FactType {
    DOG,
    CAT,
    HORSE,
    RANDOM
}
