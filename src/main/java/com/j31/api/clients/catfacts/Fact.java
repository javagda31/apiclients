package com.j31.api.clients.catfacts;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Fact {
    private boolean used;
    private String source;
    private String type;
    private boolean deleted;

    @JsonProperty("_id")
    private String id;

    private String updatedAt;
    private String createdAt;
    private String user;
    private String text;

    @JsonProperty("__v")
    private String v;

    private FactStatus status;
}
