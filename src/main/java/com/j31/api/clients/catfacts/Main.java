package com.j31.api.clients.catfacts;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        Scanner scanner = new Scanner(System.in);
        // GET      -- pobieranie
        // POST     -- aktualizacji
        // PUT      -- podmiana/wstawianie
        // DELETE   -- usuwanie
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        System.out.println("Jakiego typu ciekawostkę pobrać? [random/dog/cat/horse]");
        FactType factType = FactType.valueOf(scanner.next().toUpperCase());

        // request/zapytanie - ramka http
        HttpRequest factRequest;
        switch (factType) {
            case RANDOM:
                factRequest = createRandomFactRequest();
                break;
            case CAT:
            case DOG:
            case HORSE:
            default:
                factRequest = createFactRequest(factType);
        }
        System.out.println("Ile faktów wczytać?");
        int ilosc = scanner.nextInt();

        try {
            List<Fact> factList = new ArrayList<>();
            for (int i = 0; i < ilosc; i++) {
                HttpResponse<String> response = httpClient.send(factRequest, HttpResponse.BodyHandlers.ofString());
                String body = response.body();

                if (body.isEmpty()) {
                    i--;
                    continue;
                }
                Fact fact = objectMapper.readValue(body, Fact.class);
                factList.add(fact);
            }

            for (int i = 0; i < factList.size(); i++) {
                System.out.println((i + 1) + ". " + factList.get(i).getText());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private static HttpRequest createRandomFactRequest() {
        return HttpRequest
                .newBuilder()
                .GET()
                .uri(URI.create("https://cat-fact.herokuapp.com/facts/random"))
                .build();
    }

    private static HttpRequest createFactRequest(FactType animalType) {
        return HttpRequest
                .newBuilder()
                .GET()
                .uri(URI.create("https://cat-fact.herokuapp.com/facts/random?animal_type=" + String.valueOf(animalType).toLowerCase()))
                .build();
    }
}
