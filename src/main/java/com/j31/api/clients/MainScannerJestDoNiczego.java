package com.j31.api.clients;

import java.util.Scanner;

public class MainScannerJestDoNiczego {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Pytanie 1:");
        String wartosc1 = scanner.next();
        System.out.println(wartosc1);

        System.out.println("Pytanie 2:");
        String wartosc2 = scanner.next();
        System.out.println(wartosc2);

        System.out.println("Pytanie 3:");
        String wartosc3 = scanner.next();
        System.out.println(wartosc3);

        System.out.println("Pytanie 4:");
        String wartosc4 = scanner.next();
        System.out.println(wartosc4);

        System.out.println("Pytanie 5:");
        String wartosc5 = scanner.nextLine();
        System.out.println("Tu:->" + wartosc5 + "<-");

        System.out.println("Pytanie 6:");
        String wartosc6 = scanner.next();
        System.out.println(wartosc6);


    }
}
