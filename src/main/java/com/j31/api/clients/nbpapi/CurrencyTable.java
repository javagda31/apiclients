package com.j31.api.clients.nbpapi;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CurrencyTable {
    private String table;
    private String currency;
    private String code;

    private List<CurrencyRate> rates;
}
